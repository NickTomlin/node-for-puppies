var http = require('http');
var stream = require('stream');
var through = require('through');
// var request = require('request');

var Readable = stream.Readable;


var cap = through(function(data){
    console.log('through!');
    this.queue(data.toString().toUpperCase());
});


var config = {
    port: 3030
};


http.createServer( function (req, resp){
   req.pipe(process.stdout);
   resp.writeHead(200);
   resp.end()
}).listen(config.port, function (){
    console.log('listening at %s', config.port);
});
