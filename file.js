var fs = require('fs');
var path = require('path');
var through = require('through');


catify = new through(function(data){
    this.queue(data.toString().replace(/(!|dog|bark|woof|ball|chase)/gi, this.replacer));
});


catify.replacer = function (match) {
	words = {
		"!": " meh.",
		"dog": "cat",
		"bark": "meow",
		"woof": "meow",
		"ball": "yarn",
		"chase": "sit"
	}
	return words[match.toLowerCase()];
};


var reader = fs.createReadStream('dogDiary.txt');
var writer = fs.createWriteStream(path.normalize('generated/catDiary.txt'));

reader.pipe(catify).pipe(writer)
